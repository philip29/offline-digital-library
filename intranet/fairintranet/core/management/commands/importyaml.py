import logging
import os
import sys
import yaml


from django.core.files.images import ImageFile
from django.core.management.base import BaseCommand
from wagtail.images.models import Image

from core import models
from core.management.commands.exportyaml import type_map


logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = (
        "Reverse command: exportyaml"
        "\n\n"
        "Imports a folder structure with yaml descriptors, thumbnails and\n"
        "icons."
        "\n\n"
        "Takes the path for"
    )
    args = '<target dir> <media drive>'

    def add_arguments(self, parser):
        parser.add_argument('source', type=str,)
        parser.add_argument(
            '--overwrite',
            action='store_true',
            help="Overwrites everything that's already in the database",
        )
        parser.add_argument(
            '--skip-media-check',
            action='store_true',
            dest='skip_media_check',
            help="Does not check if media exists on disk",
        )
        parser.add_argument(
            '--media-root',
            type=str,
            default="/media/FAIR",
            dest='media_root',
            help="Location of media to check if exists for each import's field 'disk_path_check'.",
        )

    def handle(self, *args, **options):
        
        if not os.path.isdir(options['source']):
            logger.error("Source not found: {:s}".format(options['source']))
            sys.exit(-1)
        
        root_home_pages = models.HomePage.objects.exclude(sites_rooted_here=None)
        if root_home_pages.count() > 1:
            raise Exception("Non-implemented behavior: More than one root homepage")
        
        home_page = root_home_pages.first()
        self.iterate_folder(
            options['source'],
            home_page,
            overwrite=options['overwrite'],
            skip_check=options['skip_media_check'],
            media_root=options['media_root'],
        )

    def iterate_folder(self, folder, parent_resource, overwrite=False, skip_check=False, media_root="/media/FAIR"):

        logger.info("Traversing folder {}/".format(folder))

        created_resources = {}
        for name in os.listdir(folder):
            path = os.path.join(folder, name)
            if os.path.isdir(path):
                continue
            if name.endswith(".yml"):
                slug = name[:-4]  # This removes ".yml"
                logger.info("Importing {}".format(name))
                try:
                    instance = self.create_resource(
                        parent_resource,
                        folder,
                        slug,
                        overwrite=overwrite
                    )
                    created_resources[slug] = instance
                except KeyError as e:
                    logger.error("Error in .yml file. Could not find key: {}".format(e.args[0]))
                
        for name in os.listdir(folder):
            path = os.path.join(folder, name)
            if not os.path.isdir(path):
                continue
            if not name + ".yml" in os.listdir(folder):
                logger.warn("{} does not have a YAML descriptor".format(name))
                continue
            self.iterate_folder(
                os.path.join(folder, name),
                created_resources[name],
                overwrite=overwrite,
                skip_check=False,
                media_root="/media/FAIR",
            )

    def create_resource(self, parent_resource, parent_folder, name, overwrite=False, skip_check=False, media_root="/media/FAIR"):

        dct = yaml.full_load(
            open(os.path.join(parent_folder, name) + ".yml", "r")
        )
        
        type_name = dct['type']
        
        if type_name not in type_map:
            raise RuntimeError("YAML file had unknown type: {}".format(type_name))
        
        if not skip_check and dct.get('disk_path_check', None):
            path_check = os.path.join(media_root, dct['disk_path_check'])
            if not path_check.exists():
                logger.warning(
                    "Skipping YAML import as path does not exist: {}"
                )
                return
        
        ModelClass = type_map[type_name]
        
        try:
            instance = ModelClass.objects.child_of(parent_resource).get(
                source_yaml_name=name
            )
            if not overwrite:
                logger.info("Not overwriting: {}".format(name))
                return instance
        except ModelClass.DoesNotExist:
            path = parent_resource.path + "{pos:s}".format(pos=str(parent_resource.numchild + 1).zfill(4))
            instance = ModelClass(path=path)
            instance.url_path = os.path.join(parent_resource.url_path, name) + "/"
            instance.source_yaml_name = name
        
        known_keys = [f.name for f in ModelClass._meta.get_fields()]

        for key in [k for k in dct.keys() if k in known_keys]:
            setattr(instance, key, dct[key])

        if not instance.id:
            instance.slug = name
            instance.numchild = 0
            instance.depth = parent_resource.depth + 1
            instance.show_in_menus = False
            parent_resource.numchild += 1
            parent_resource.save()
            others = parent_resource.get_children().filter(slug=name)
            if others.exists():
                logger.warn("Other objects with same path existed and were deleted. Slug: {}".format(name))
                others.delete()
        else:
            others = parent_resource.get_children().filter(slug=instance.slug).exclude(id=instance.id)
            if others.exists():
                logger.warn("Other objects with same path existed and were deleted. Slug: {}".format(name))
                others.delete()
        
        # Save the instance before adding related objects (icons and thumbnails)
        instance.save()
        logger.info("Saved instance: {}".format(instance))
        
        if 'icon_file' in dct and dct['icon_file']:
            
            path = os.path.join(parent_folder, dct['icon_file'])
            
            if not os.path.isfile(path):
                raise RuntimeError("Icon {} does not exist on disk".format(path))
            
            if not instance.icon:
                icon = Image()
            else:
                icon = instance.icon
            f = open(path, 'rb')
            icon.file = ImageFile(f, name=dct['icon_file'])
            icon.title = "Icon for " + instance.title
            icon.save()
            instance.icon = icon
            
        if 'thumbnail_file' in dct and dct['thumbnail_file']:
            path = os.path.join(parent_folder, dct['thumbnail_file'])
            
            if not os.path.isfile(path):
                raise RuntimeError("Thumbnail {} does not exist on disk".format(path))
            
            if not instance.thumbnail:
                thumbnail = Image()
            else:
                thumbnail = instance.thumbnail
            f = open(path, 'rb')
            thumbnail.file = ImageFile(f, name=dct['thumbnail_file'])
            thumbnail.title = "Thumbnail for " + instance.title
            thumbnail.save()
            instance.thumbnail = thumbnail
        
        instance.save()
        return instance
