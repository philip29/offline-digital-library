#!/bin/bash

if [ ! -d $FAIR_DRIVE_MOUNTPOINT/data/kolibri ]
then
	echo "Not on drive: $FAIR_DRIVE_MOUNTPOINT/data/kolibri"
	return
fi

if ! [ -e $FAIR_DRIVE_MOUNTPOINT/repository/kolibri ]
then
	echo 'Missing Kolibri repository: $FAIR_DRIVE_MOUNTPOINT/repository/kolibri'
	return
fi

echo "---------------------------------------"
echo "Kolibri"
echo "---------------------------------------"

if ! [ -e /etc/apt/sources.list.d/kolibri.list ]
then
	echo "Adding kolibri repository and updating apt..."
	echo ""
	echo "deb file://$FAIR_DRIVE_MOUNTPOINT/repository/kolibri/ubuntu bionic main" > /etc/apt/sources.list.d/kolibri.list
	apt update
fi

echo "Copying in the .kolibri data directory"
if [ ! -d /home/kolibri ]
then
	echo "Creating a kolibri user"
	useradd -m -U kolibri
fi
#tar xvz -f $FAIR_DRIVE_MOUNTPOINT/data/kolibri/kolibri_home.tar.gz -C /home/kolibri
#chown -R kolibri /home/kolibri/.kolibri
#chmod -R o+wX /home/kolibri/.kolibri

copy_skel etc/nginx/sites-available/003-kolibri
copy_skel etc/nginx/sites-enabled/003-kolibri

echo "Installing Kolibri deb pkg"
echo "kolibri kolibri/init select false" | debconf-set-selections
echo "kolibri kolibri/user string kolibri" | debconf-set-selections
echo "kolibri-server kolibri-server/port select 8080" | debconf-set-selections

# The * is because some drives may contain 0.17.0 and some 0.17.4
DEBIAN_FRONTEND=noninteractive apt install -y kolibri
DEBIAN_FRONTEND=noninteractive apt install -y kolibri-server

